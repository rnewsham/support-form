package Support::Form::View::HTML;
use Moose;
use namespace::autoclean;

extends 'Catalyst::View::TT';

__PACKAGE__->config(
    TEMPLATE_EXTENSION => '.tt',
    render_die => 1,
    WRAPPER => 'wrapper.tt',
    ENCODING => 'UTF-8',

);

=head1 NAME

Support::Form::View::HTML - TT View for Support::Form

=head1 DESCRIPTION

TT View for Support::Form.

=head1 SEE ALSO

L<Support::Form>

=head1 AUTHOR

Richard Newsham

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
