# Support-Form

An MVP support request system.

## Overview

This is built using catalyst and runs inside a docker/podman container.

The request forms are defined in a json config format in Support-Form/support_config.json along with a list of clients. The email from and to details are in the Support-Form/support_form.conf file.

The email objects are written to the Support-Form/email directory which should be bind mounted outside of the container.

There is a request_log file written to Support-Form/logs directory which should also be bind mounted outside the container. A json blob of each requests details is appended to the request_log. 

## Running

### Clone the repo
```
$ git clone git@gitlab.com:rnewsham/support-form.git
```

### Run with Podman
```
$ podman build -t support:latest .
$ podman run -p 3000:3000 --mount type=bind,src=logs,target=/Support-Form/logs --mount type=bind,src=emails,target=/Support-Form/emails support:latest
or
$ docker build -t support:latest .
$ docker run -p 3000:3000 --mount type=bind,src=logs,target=/Support-Form/logs --mount type=bind,src=emails,target=/Support-Form/emails support:latest
```

### Access in browser

The application will be accessible at http://localhost:3000

## Future improvements

- Better config handling, passing all config from outside container.
- Database for storing clients list, form data and submitted requests
- Improve error handling, more user friendly error messages
- Better form validation allow defining of required fields

