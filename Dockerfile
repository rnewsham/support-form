FROM debian:latest

RUN apt update

RUN apt install -y libcatalyst-perl \
	libcatalyst-devel-perl \
	libcatalyst-plugin-static-simple-perl \
	libcatalyst-plugin-configloader-perl \
	libjson-perl \
	libcatalyst-view-tt-perl \
	libcatalyst-action-renderview-perl \
	libfile-mimeinfo-perl \
	starman

COPY ./Support-Form /Support-Form

WORKDIR /Support-Form

ENTRYPOINT ["/Support-Form/script/support_form_server.pl", "-f"]
