#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use Catalyst::Test 'Support::Form';

ok( request('/')->is_success, 'Request should succeed' );

done_testing();
