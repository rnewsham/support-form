use strict;
use warnings;

use Support::Form;

my $app = Support::Form->apply_default_middlewares(Support::Form->psgi_app);
$app;

