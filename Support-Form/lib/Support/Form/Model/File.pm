package Support::Form::Model::File;

use Moose;
use namespace::autoclean;
    
use Data::Dumper;
use File::MimeInfo;

BEGIN { extends 'Catalyst::Model' }
    
use File::Path qw(make_path);

sub upload
{
    my ( $self, $args ) = @_;
    my $filename = $args->{upload}->filename;
    my $target = $args->{upload_dir};

    make_path($target);
    $filename = time . '_' . $filename;
    $args->{upload}->copy_to("$target/$filename");
	my $mime_type = mimetype("$target/$filename");
    return $filename, $mime_type;
}

__PACKAGE__->meta->make_immutable;

1;

