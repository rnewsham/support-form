package Support::Form::Controller::Support;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

use JSON;
use Data::Dumper;

=head1 NAME

Support::Form::Controller::Support - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub base :Chained('/') :PathPart('support') :CaptureArgs(0) 
{
	my ( $self, $c ) = @_;

	#FIXME Really should be loaded at launch but loading here to start with to allow editing without a restart" 
	open my $fh, "<",  $c->config->{home} . "/support_config.json" or die "Could not load support_config.json $!";
	local $/ = '';
	my $json = <$fh>;
	close $fh;
	
	my $config;
	eval {
		$config = from_json( $json );
	};
	if ( $@ ) 
	{
		die "Error parsing config $@";
	}
	$c->stash( config => $config );
}


sub index :Chained('base') :PathPart('') :Args(0) 
{
    my ( $self, $c ) = @_;
}

sub request :Chained('base') :PathPart('request') :Args(0) 
{
	my ( $self, $c ) = @_;

	$c->stash( request_id => $c->req->params->{request_id} );

	my $request = $c->stash->{config}->{requests}->[$c->req->params->{request_id}];

	if ( ref $request eq 'HASH' and $request->{title} )	
	{
		$c->stash( request => $request );
		
		if ( $c->req->params->{submit_request} ) 
		{
			my %submission;
			
			push @{ $submission{fields} }, { field => 'Client', value => $c->req->params->{client} };
		
			for ( @{ $request->{fields} } )
			{
				if ( $_->{type} eq 'file' )
				{
					if ( my $upload = $c->req->upload( $_->{name} ) )
					{
						my ( $filename, $mimetype ) = $c->model('File')->upload( {
							upload => $upload,
							upload_dir => $c->config->{home} . '/attachments',
						} );
						push @{ $submission{attachments} }, { file => $filename, 'mime-type' => $mimetype };
					}
				}
				else
				{
					if ( $_->{validation} )
					{
						if ( $c->req->params->{$_->{name}} =~ /$_->{validation}/ )
						{
							push @{ $submission{fields} }, { field => $_->{label}, value => $c->req->params->{$_->{name}} };	
						}
						else
						{	
							$c->stash( error => 'Invalid Field ' . $_->{label} );
							return;
						}
					}
					else
					{
							push @{ $submission{fields} }, { field => $_->{label}, value => $c->req->params->{$_->{name}} };	
					}
				}
			}
			warn Dumper( $c->config );	
			if ( $c->model( 'Request' )->create( $c->config->{email}, \%submission ) ) 
			{
				$c->stash( success => 1 );
			} 
			else 
			{
				$c->stash( error => 'Invalid request' );
			}
		}
	}
	else 
	{
		$c->stash( error => 'Invalid request' );
	}
}

=encoding utf8

=head1 AUTHOR

Richard Newsham

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
