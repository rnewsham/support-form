use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Support::Form';
use Support::Form::Controller::Support;

ok( request('/support')->is_success, 'Request should succeed' );
done_testing();
