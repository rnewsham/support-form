#!/usr/bin/env perl

use strict;
use warnings;

use Test::Spec;
use Catalyst::Test 'Support::Form';

use JSON;


my $config;
describe "Can load support form config" => sub {

	it "file should exist" => sub {
        ok( -e 'support_config.json' );
    };
	
	local $/ = '';
	open my $fh, '<', 'support_config.json' or die $!;
	my $json = <$fh>;	

	it "file should be valid json" => sub {
        ok( $config = from_json( $json ) );
    };

	it "is a hash" => sub {
		is( ref $config, 'HASH' );
	};

	it "has array of requests" => sub {
		is( ref $config->{requests}, 'ARRAY' );
	};
	
	it "has array of clients" => sub {
		is( ref $config->{clients}, 'ARRAY' );
	};
};


describe "/support renders" => sub {
	my $content;
	before 'all' => sub {
		$content = get( '/support' );
	};
	
	it "it returns success" => sub {
		ok( request('/support')->is_success );
	};

	it "has content" => sub {
		ok( $content );	
	};

	it "content matches expected" => sub {
		like( $content, qr/<option value='0'>$config->{requests}->[0]->{title}<\/option>/ );
	};	
};

runtests unless caller;
