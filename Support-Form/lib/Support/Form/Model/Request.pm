package Support::Form::Model::Request;
use Moose;
use namespace::autoclean;

use JSON;

extends 'Catalyst::Model';

=head1 NAME

Support::Form::Model::Request - Catalyst Model

=head1 DESCRIPTION

Catalyst Model.


=encoding utf8

=head1 AUTHOR

Richard Newsham

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut


sub create
{
	my ( $self, $config, $submission ) = @_;
	$self->_log( $submission );
	$self->_email( $config, $submission );
}

sub _email
{
	my ( $self, $config, $submission ) = @_;

	my %email = (
		to => $config->{to},
		from => $config->{from},
		subject => $config->{subject},
		attachments => $submission->{attachments},
	);

	for ( @{ $submission->{fields} } ) 
	{
		$email{body} .= $_->{field} . ': ' . $_->{value} . '<br />';
	}

	my $json;
	eval {
		$json = to_json( \%email );
	};

	if ( $@ ) 
	{
		die "Couldn't encode email object $@";
	}

	my $time = time;
	open my $fh, '>', "emails/$time.json" or die "Could not write email object $!";
	print $fh $json;
	close $fh;
 
=head
	{
		"to" : ​ "foo@bar.com"​ ,
		"from" : ​ "foo@bar.com"​ ,
		"subject" : ​ "something"​ ,
		"body" : ​ "something"​ ,
		"attachments" : [
			{"mime-type" : ​ "text/plain"​ ,
			"file" : ​ "/tmp/xcnmxcz"​ }
		]
	}
=cut
}

sub _log
{
	my ( $self, $submission ) = @_;
	my $json;
	$submission->{time} = time;
	eval {
		$json = to_json( $submission );
	};
	if ( $@ )
	{
		return to_json( { error => $@ } );
	}
	open my $fh, '>>', 'logs/request_log' or die "Unable to open log $!";
	print $fh $json . "\n";
	close $fh;
}

__PACKAGE__->meta->make_immutable;

1;
